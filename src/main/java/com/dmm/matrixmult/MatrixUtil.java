package com.dmm.matrixmult;

import java.util.Arrays;

public class MatrixUtil {

	public static final int MAX_MATRIX_SIZE = 4;
	public static final int[][] EMPTY_MATRIX = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];

	public static int[][] multiplyBlock(int[][] a, int[][] b) {
		final int rowCount = a.length;
		int[][] c = new int[rowCount][rowCount];

		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < rowCount; j++)
				for (int k = 0; k < rowCount; k++)
					c[i][j] += a[i][k] * b[k][j];

		return c;
	}

	public static int[][] multiply2x2Block(int[][] a, int[][] b) {
		int[][] c = new int[2][2];
		c[0][0] = a[0][0] * b[0][0] + a[0][1] * b[1][0];
		c[0][1] = a[0][0] * b[0][1] + a[0][1] * b[1][1];
		c[1][0] = a[1][0] * b[0][0] + a[1][1] * b[1][0];
		c[1][1] = a[1][0] * b[0][1] + a[1][1] * b[1][1];
		return c;
	}

	public static int[][] transpose(int[][] a) {
		final int rowCount = a.length;
		final int[][] b = new int[rowCount][rowCount];
		for (int i = 0; i < rowCount; i++)
			for (int j = 0; j < rowCount; j++)
				b[i][j] = a[j][i];

		return b;
	}

	public static int[][] add2x2Block(int[][] a, int[][] b) {
		int[][] c = new int[2][2];

		c[0][0] = a[0][0] + b[0][0];
		c[0][1] = a[0][1] + b[0][1];
		c[1][0] = a[1][0] + b[1][0];
		c[1][1] = a[1][1] + b[1][1];

		return c;
	}

	public static int[][] addBlock(int[][] a, int[][] b) {
		final int rowCount = a.length;
		int[][] c = new int[rowCount][rowCount];
		for (int i = 0; i < c.length; i++) {
			for (int j = 0; j < c.length; j++) {
				c[i][j] = a[i][j] + b[i][j];
			}
		}
		return c;
	}

	// https://graphics.stanford.edu/~seander/bithacks.html#DetermineIfPowerOf2
	public static boolean isNotPowerOfTwo(int val) {
		return val == 0 || (val & (val - 1)) != 0;
	}

	public static boolean isNotSquareMatrix(Matrix mat) {
		final int rowCount = mat.getRowCount();
		for (MatRow row : mat.getRowList())
			if (row.getValCount() != rowCount)
				return true;

		return false;
	}

	public static boolean isNotSquareMatrix(int[][] mat) {
		final int rowCount = mat.length;
		for (int[] row : mat)
			if (row.length != rowCount)
				return true;

		return false;
	}

	public static String mat2dArrayToString(int[][] mat) {
		final StringBuilder buff = new StringBuilder("{\n");

		for (int i = 0; i < mat.length; i++) {
			buff.append("    ").append(Arrays.toString(mat[i])).append("\n");
		}

		buff.append("}");

		return buff.toString();
	}

}
