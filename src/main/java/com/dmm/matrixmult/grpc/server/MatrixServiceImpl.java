package com.dmm.matrixmult.grpc.server;

import com.dmm.matrixmult.MatrixUtil;
import com.dmm.matrixmult.MatrixOpRequest;
import com.dmm.matrixmult.MatrixOpResponse;
import com.dmm.matrixmult.MatrixServiceGrpc.MatrixServiceImplBase;

import com.dmm.matrixmult.grpc.GrpcUtil;
import io.grpc.stub.StreamObserver;

import java.util.function.BiFunction;

class MatrixServiceImpl extends MatrixServiceImplBase {

	private static void executeMatOp(MatrixOpRequest req, StreamObserver<MatrixOpResponse> res, BiFunction<int[][], int[][], int[][]> function) {
		System.out.println("Request received from client:");

		int[][] c = MatrixUtil.EMPTY_MATRIX;
		try {
			final int[][] a = GrpcUtil.get2dArray(req.getA());
			final int[][] b = GrpcUtil.get2dArray(req.getB());

			c = function.apply(a, b);

			System.out.println("A:" +
					MatrixUtil.mat2dArrayToString(a) +
					"\n\nB:" +
					MatrixUtil.mat2dArrayToString(b)
			);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Received an invalid matrix. Returning the empty matrix.");
		}

		final MatrixOpResponse mulRes = MatrixOpResponse.newBuilder().setC(GrpcUtil.getMatrix(c)).build();

		res.onNext(mulRes);
		res.onCompleted();
	}

	@Override
	public void mulBlock(MatrixOpRequest req, StreamObserver<MatrixOpResponse> res) {
		executeMatOp(req, res, MatrixUtil::multiplyBlock);
	}

	@Override
	public void addBlock(MatrixOpRequest req, StreamObserver<MatrixOpResponse> res) {
		executeMatOp(req, res, MatrixUtil::addBlock);
	}

}

