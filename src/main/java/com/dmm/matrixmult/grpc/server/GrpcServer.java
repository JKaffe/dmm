package com.dmm.matrixmult.grpc.server;

import java.io.IOException;

import com.dmm.matrixmult.grpc.server.MatrixServiceImpl;
import io.grpc.Server;
import io.grpc.ServerBuilder;

public class GrpcServer {
	final static int port = 8081;

	public static void main(String[] args) throws IOException, InterruptedException {
		Server server = ServerBuilder
				.forPort(port)
				.addService(new MatrixServiceImpl())
				.build();

		System.out.println("Starting server on port " + port + " ...");
		server.start();
		System.out.println("Server started!");
		server.awaitTermination();
	}
}
