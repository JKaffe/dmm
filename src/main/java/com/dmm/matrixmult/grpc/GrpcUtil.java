package com.dmm.matrixmult.grpc;

import com.dmm.matrixmult.*;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GrpcUtil {

	public static MatrixOpRequest boxMatrixRequest(int[][] a, int[][] b) {
		return MatrixOpRequest.newBuilder()
				.setA(getMatrix(a))
				.setB(getMatrix(b))
				.build();
	}

	// gRPC returns a ListenableFuture - Use standard Java CompletableFuture.
	public static CompletableFuture<int[][]> getCompletableFutureWrapper(ListenableFuture<MatrixOpResponse> fReq, ExecutorService executorService) {

		CompletableFuture<MatrixOpResponse> cf = new CompletableFuture<MatrixOpResponse>() {
			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				boolean res = fReq.cancel(mayInterruptIfRunning);
				super.cancel(mayInterruptIfRunning);
				return res;
			}
		};

		Futures.addCallback(fReq, new FutureCallback<MatrixOpResponse>() {
			@Override
			public void onSuccess(MatrixOpResponse result) {
				cf.complete(result);
			}

			@Override
			public void onFailure(Throwable t) {
				cf.completeExceptionally(t);
			}
		}, executorService);

		return cf.thenApplyAsync((MatrixOpResponse res) -> get2dArray(res.getC()), executorService);
	}

	public static int[][] get2dArray(Matrix mat) {
		final int rowCount = mat.getRowCount();
		if (rowCount == 0)
			throw new IllegalArgumentException("Matrix is empty.");

		if (MatrixUtil.isNotSquareMatrix(mat))
			throw new IllegalArgumentException("Not a square matrix.");

		// Convert a MatRow to an int array. MatRow.getValList() returns List<Integer>.
		final Function<MatRow, int[]> rowToArray = row -> row.getValList().stream()
				.sequential()
				.mapToInt(Integer::intValue)
				.toArray();

		return mat.getRowList().stream()
				.sequential()
				.map(rowToArray)
				.toArray(size -> new int[size][size]);
	}

	public static Matrix getMatrix(int[][] arrMat) {
		if (arrMat.length == 0)
			throw new IllegalArgumentException("Matrix is empty.");

		if (MatrixUtil.isNotSquareMatrix(arrMat))
			throw new IllegalArgumentException("Not a square matrix.");

		final List<MatRow> rows = new LinkedList<>();
		for (int[] arrRow : arrMat) {
			final List<Integer> values = IntStream.of(arrRow).sequential()
					.boxed()
					.collect(Collectors.toList());

			rows.add(MatRow.newBuilder().addAllVal(values).build());
		}

		return Matrix.newBuilder().addAllRow(rows).build();
	}
}
