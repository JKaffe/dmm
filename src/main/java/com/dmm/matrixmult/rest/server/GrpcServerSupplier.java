package com.dmm.matrixmult.rest.server;

import java.util.List;
import java.util.function.Supplier;

public class GrpcServerSupplier implements Supplier<GrpcServerPojo> {
	private final GrpcServerPojo[] servers;
	private int couner = 0;

	protected GrpcServerSupplier(List<GrpcServerPojo> servers) {
		this.servers = servers.toArray(new GrpcServerPojo[servers.size()]);
	}

	@Override
	public GrpcServerPojo get() {
		final GrpcServerPojo server = this.servers[couner];
		couner = (couner + 1) % servers.length;
		return server;
	}
}
