package com.dmm.matrixmult.rest.server;

import com.dmm.matrixmult.MatrixUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

@RestController
public class MatrixRestController {

	@Autowired(required = false)
	private GrpcServerPool serverPool;

	@PostMapping("/multiply")
	protected int[][] multiply(@RequestParam Optional<Integer> deadlineParam, @RequestBody MatrixRpcBinOp matrices) {
		final int[][] a = matrices.getA();
		final int[][] b = matrices.getB();
		final Optional<Duration> deadline = deadlineParam.flatMap(ms -> Optional.of(Duration.ofMillis(ms)));

		// Validate input.
		if (a.length != b.length ||
			MatrixUtil.isNotPowerOfTwo(a.length) ||
			MatrixUtil.isNotPowerOfTwo(b.length) ||
			MatrixUtil.isNotSquareMatrix(a) ||
			MatrixUtil.isNotSquareMatrix(b))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Only square matrices of equal dimensions and with dimensions " +
							"of powers of 2 are accepted.");


		System.out.println("Multiplying:\nA:" +
				MatrixUtil.mat2dArrayToString(a) +
				"\nB:" +
				MatrixUtil.mat2dArrayToString(b));

		final DistributedMatrixMultiplication mulOp = new DistributedMatrixMultiplication(a, b);

		// Handling requests asynchronously.
		final int[][] res;
		try {
			res = this.serverPool.submit(mulOp, deadline);
		} catch (InterruptedException | ExecutionException e){
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,
					"Request could not be processed.");
		} catch (TimeoutException e) {
			throw new ResponseStatusException(HttpStatus.REQUEST_TIMEOUT);
		}
		return res;
	}

	@PostMapping("/multiplyAsync")
	protected Mono<int[][]> multiplyAsync(@RequestParam Optional<Integer> deadlineParam, @RequestBody MatrixRpcBinOp matrices) {
		final int[][] a = matrices.getA();
		final int[][] b = matrices.getB();
		final Optional<Duration> deadline = deadlineParam.flatMap(ms -> Optional.of(Duration.ofMillis(ms)));

		// Validate input.
		if (a.length != b.length ||
				MatrixUtil.isNotPowerOfTwo(a.length) ||
				MatrixUtil.isNotPowerOfTwo(b.length) ||
				MatrixUtil.isNotSquareMatrix(a) ||
				MatrixUtil.isNotSquareMatrix(b))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					"Only square matrices of equal dimensions and with dimensions " +
							"of powers of 2 are accepted.");


		System.out.println("Multiplying:\nA:" +
				MatrixUtil.mat2dArrayToString(a) +
				"\nB:" +
				MatrixUtil.mat2dArrayToString(b));

		final DistributedMatrixMultiplication mulOp = new DistributedMatrixMultiplication(a, b);

		// Handling requests asynchronously.
		final CompletableFuture<int[][]> res;
		res = this.serverPool.submitAsync(mulOp, deadline);
		return Mono.fromFuture(res);
	}

}
