package com.dmm.matrixmult.rest.server;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@SpringBootApplication
public class RestServer {
	@Autowired
	private ApplicationContext appContext;
	private GrpcServerPool serverPool;

	@Bean
	public GrpcServerPool getServerPool(ApplicationArguments appArgs) {
		String[] args = appArgs.getSourceArgs();
		final List<String> servers = new ArrayList<>();
		if (args.length > 1 && args[0].equals("-s")) {
			servers.addAll(
					Arrays.asList(
							Arrays.copyOfRange(args, 1, args.length)).parallelStream()
							.map(String::trim)
							.collect(Collectors.toList())
			);

			// TODO: arg validation can be improved, e.g. check for x > 255
			// TODO: handle "localhost"
			List<String> invalidArgs = servers.parallelStream()
					.filter(arg -> !arg.matches("^(\\d{1,3}\\.){3}\\d{1,3}:\\d{1,5}$"))
					.collect(Collectors.toList());

			if (invalidArgs.size() > 0) {
				// TODO: Handle invalid args more gracefully - skip the invalid ones.
				final Logger logger = Logger.getGlobal();
				invalidArgs.forEach(arg -> logger.log(Level.SEVERE, "Invalid arg: \"" + arg + "\"."));
				SpringApplication.exit(appContext, () -> 1);
			}

		} else {
			//TODO: Remove this.
			servers.add("127.0.0.1:8081");
		}

		// Create grpc server pojos from cmd args.
		final List<GrpcServerPojo> serverPojos = servers.parallelStream()
				.map(server -> {
					final String[] ipAndPort = server.split(":");
					return new GrpcServerPojo(ipAndPort[0], Integer.parseInt(ipAndPort[1]));
				})
				.collect(Collectors.toList());

		this.serverPool = new GrpcServerPool(serverPojos);
		return this.serverPool;
	}

	public static void main(String[] args) {
		SpringApplication.run(RestServer.class, args);
	}
}
