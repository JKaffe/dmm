package com.dmm.matrixmult.rest.server;

import com.dmm.matrixmult.MatrixServiceGrpc;
import com.google.common.collect.Lists;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.dmm.matrixmult.grpc.GrpcUtil.boxMatrixRequest;
import static com.dmm.matrixmult.grpc.GrpcUtil.getCompletableFutureWrapper;

public class GrpcServerPool implements AutoCloseable {
	public static final Duration DEFAULT_DEADLINE_DURATION = Duration.ofSeconds(90);

	private final List<GrpcServerPojo> servers;
	private final ExecutorService executorService;
	private double footprint = 1.0;

	protected GrpcServerPool(List<GrpcServerPojo> servers) {
		this.servers = Lists.newArrayList(servers);
		this.executorService = Executors.newCachedThreadPool();

		final StringBuilder logStrBuilder = new StringBuilder()
				.append(GrpcServerPool.class.getSimpleName())
				.append(" created with: ");
		this.servers.parallelStream().
				map(server -> server.getName() + " ")
				.forEach(logStrBuilder::append);
		Logger.getGlobal().log(Level.INFO, logStrBuilder.toString());
	}

	protected GrpcServerSupplier allocateServer(Optional<Duration> deadline) {
		final int numOfServers = this.servers.size();
		final long duration = deadline.orElse(DEFAULT_DEADLINE_DURATION).getSeconds();

		final Logger globalLogger = Logger.getGlobal();
		deadline.ifPresent(t -> globalLogger.log(Level.INFO, GrpcServerPool.class.getSimpleName() +
				" requested allocation for deadline: " + t.getSeconds() + " seconds."));

		// Only considering 8 multiplications for a 4x4 matrix.
		final int numOfServersToAllocate = new Double(
				Math.min(Math.max(8/(footprint * duration), 1), numOfServers)
		).intValue();

		// TODO: Perhaps pick the fastest servers, for a shorter deadline.
		List<GrpcServerPojo> servers = this.servers.parallelStream()
				.limit(numOfServersToAllocate)
				.collect(Collectors.toList());

		final StringBuilder logStrBuilder = new StringBuilder()
				.append(GrpcServerPool.class.getSimpleName())
				.append(" allocated the following gRPC servers to the request: ");
		servers.stream()
				.map(server -> server.getName() + " ")
				.forEach(logStrBuilder::append);

		globalLogger.log(Level.INFO, logStrBuilder.toString());
		return new GrpcServerSupplier(servers);
	}

	protected int[][] submit(
			DistributedMatrixMultiplication multiplication,
			Optional<Duration> deadline
	)
			throws InterruptedException, ExecutionException, TimeoutException
	{
		final GrpcServerSupplier servers = this.allocateServer(deadline);
		return multiplication.matrixMultiplyDistributed(servers, this.executorService);
	}

	// TODO: Fix. Async related.
	protected CompletableFuture<int[][]> submitAsync(
			DistributedMatrixMultiplication multiplication,
			Optional<Duration> deadline
	)
	{
		final GrpcServerSupplier servers = this.allocateServer(deadline);
		return multiplication.asyncMatrixMultiplyDistributed(servers, this.executorService);
	}

	@Override
	public void close() {
		this.servers.forEach(GrpcServerPojo::close);
	}
}
