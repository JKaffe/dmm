package com.dmm.matrixmult.rest.server;

import java.util.concurrent.*;
import java.util.function.BiFunction;

import static com.dmm.matrixmult.grpc.GrpcUtil.boxMatrixRequest;
import static com.dmm.matrixmult.grpc.GrpcUtil.getCompletableFutureWrapper;

public class DistributedMatrixMultiplication extends MatrixRpcBinOp {
	public static final int MAX_MATRIX_SIZE = 4;

	public DistributedMatrixMultiplication(int[][] a, int[][] b) {
		super(a, b);
	}

	public int[][] matrixMultiplyDistributed(GrpcServerSupplier serverSupplier, ExecutorService executorService)
			throws InterruptedException, ExecutionException, TimeoutException
	{
		final int[][] a = super.getA();
		final int[][] b = super.getB();
		final int rowCount = a.length;
		final int bSize = rowCount/2;

		final BiFunction<int[][], int[][], CompletableFuture<int[][]>> blockAdderFunction =
				(x, y) -> getCompletableFutureWrapper(serverSupplier.get().getFutureStub().addBlock(boxMatrixRequest(x, y)), executorService);

		final BiFunction<int[][], int[][], CompletableFuture<int[][]>> blockMultiplierFunction =
				(x, y) -> getCompletableFutureWrapper(serverSupplier.get().getFutureStub().mulBlock(boxMatrixRequest(x, y)), executorService);

		int[][] A1 = new int[bSize][bSize];
		int[][] A2 = new int[bSize][bSize];
		int[][] B1 = new int[bSize][bSize];
		int[][] B2 = new int[bSize][bSize];
		int[][] C1 = new int[bSize][bSize];
		int[][] C2 = new int[bSize][bSize];
		int[][] D1 = new int[bSize][bSize];
		int[][] D2 = new int[bSize][bSize];
		int[][] res = new int[rowCount][rowCount];

		for (int i = 0; i < bSize; i++) {
			for (int j = 0; j < bSize; j++) {
				A1[i][j] = a[i][j];
				A2[i][j] = b[i][j];
			}
		}
		for (int i = 0; i < bSize; i++) {
			for (int j = bSize; j < rowCount; j++) {
				B1[i][j - bSize] = a[i][j];
				B2[i][j - bSize] = b[i][j];
			}
		}
		for (int i = bSize; i < rowCount; i++) {
			for (int j = 0; j < bSize; j++) {
				C1[i - bSize][j] = a[i][j];
				C2[i - bSize][j] = b[i][j];
			}
		}
		for (int i = bSize; i < rowCount; i++) {
			for (int j = bSize; j < rowCount; j++) {
				D1[i - bSize][j - bSize] = a[i][j];
				D2[i - bSize][j - bSize] = b[i][j];
			}
		}

		int[][] A3, B3, C3, D3;

		if (bSize == 4) {
			A3 = blockAdderFunction.apply(
					blockMultiplierFunction.apply(A1, A2).get(3, TimeUnit.SECONDS),
					blockMultiplierFunction.apply(B1, C2).get(3, TimeUnit.SECONDS)
			).get(3, TimeUnit.SECONDS);

			B3 = blockAdderFunction.apply(
					blockMultiplierFunction.apply(A1, B2).get(3, TimeUnit.SECONDS),
					blockMultiplierFunction.apply(B1, D2).get(3, TimeUnit.SECONDS)
			).get(3, TimeUnit.SECONDS);

			C3 = blockAdderFunction.apply(
					blockMultiplierFunction.apply(C1, A2).get(3, TimeUnit.SECONDS),
					blockMultiplierFunction.apply(D1, C2).get(3, TimeUnit.SECONDS)
			).get(3, TimeUnit.SECONDS);

			D3 = blockAdderFunction.apply(
					blockMultiplierFunction.apply(C1, B2).get(3, TimeUnit.SECONDS),
					blockMultiplierFunction.apply(D1, D2).get(3, TimeUnit.SECONDS)
			).get(3, TimeUnit.SECONDS);

		} else {
			A3 = blockAdderFunction.apply(
					new DistributedMatrixMultiplication(A1, A2).matrixMultiplyDistributed(serverSupplier, executorService),
					new DistributedMatrixMultiplication(B1, C2).matrixMultiplyDistributed(serverSupplier, executorService)
			).get(3, TimeUnit.SECONDS);

			B3 = blockAdderFunction.apply(
					new DistributedMatrixMultiplication(A1, B2).matrixMultiplyDistributed(serverSupplier, executorService),
					new DistributedMatrixMultiplication(B1, D2).matrixMultiplyDistributed(serverSupplier, executorService)
			).get(3, TimeUnit.SECONDS);

			C3 = blockAdderFunction.apply(
					new DistributedMatrixMultiplication(C1, A2).matrixMultiplyDistributed(serverSupplier, executorService),
					new DistributedMatrixMultiplication(D1, C2).matrixMultiplyDistributed(serverSupplier, executorService)
			).get(3, TimeUnit.SECONDS);

			D3 = blockAdderFunction.apply(
					new DistributedMatrixMultiplication(C1, B2).matrixMultiplyDistributed(serverSupplier, executorService),
					new DistributedMatrixMultiplication(D1, D2).matrixMultiplyDistributed(serverSupplier, executorService)
			).get(3, TimeUnit.SECONDS);
		}

		for (int i = 0; i < bSize; i++) {
			for (int j = 0; j < bSize; j++) {
				res[i][j] = A3[i][j];
			}
		}
		for (int i = 0; i < bSize; i++) {
			for (int j = bSize; j < rowCount; j++) {
				res[i][j] = B3[i][j - bSize];
			}
		}
		for (int i = bSize; i < rowCount; i++) {
			for (int j = 0; j < bSize; j++) {
				res[i][j] = C3[i - bSize][j];
			}
		}
		for (int i = bSize; i < rowCount; i++) {
			for (int j = bSize; j < rowCount; j++) {
				res[i][j] = D3[i - bSize][j - bSize];
			}
		}

		return res;
	}

	// TODO: Not working - uncertain about the cause - tracebacks didn't help much.
	public CompletableFuture<int[][]> asyncMatrixMultiplyDistributed(GrpcServerSupplier serverSupplier, ExecutorService executorService) {
		final int[][] a = super.getA();
		final int[][] b = super.getB();

		final BiFunction<CompletableFuture<int[][]>, CompletableFuture<int[][]>, CompletableFuture<int[][]>> blockAdderFunction =
				(CompletableFuture<int[][]> x, CompletableFuture<int[][]> y) ->
						x.thenCompose(
								(int[][] i) -> y.thenCompose(
										(int[][] j) -> getCompletableFutureWrapper(
												serverSupplier.get().getFutureStub().addBlock(boxMatrixRequest(i,j)),
												executorService
										)
								)
						);

		final BiFunction<CompletableFuture<int[][]>, CompletableFuture<int[][]>, CompletableFuture<int[][]>> blockMultiplierFunction =
				(CompletableFuture<int[][]> x, CompletableFuture<int[][]> y) ->
						x.thenCompose(
								(int[][] i) -> y.thenCompose(
										(int[][] j) ->
												getCompletableFutureWrapper(
														serverSupplier.get().getFutureStub().mulBlock(boxMatrixRequest(i, j)),
														executorService
												)
								)
						);

		int bSize = 2;
		int[][] A1 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] A2 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] B1 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] B2 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] C1 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] C2 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] D1 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];
		int[][] D2 = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];

		for (int i = 0; i < bSize; i++) {
			for (int j = 0; j < bSize; j++) {
				A1[i][j] = a[i][j];
				A2[i][j] = b[i][j];
			}
		}
		for (int i = 0; i < bSize; i++) {
			for (int j = bSize; j < MAX_MATRIX_SIZE; j++) {
				B1[i][j - bSize] = a[i][j];
				B2[i][j - bSize] = b[i][j];
			}
		}
		for (int i = bSize; i < MAX_MATRIX_SIZE; i++) {
			for (int j = 0; j < bSize; j++) {
				C1[i - bSize][j] = a[i][j];
				C2[i - bSize][j] = b[i][j];
			}
		}
		for (int i = bSize; i < MAX_MATRIX_SIZE; i++) {
			for (int j = bSize; j < MAX_MATRIX_SIZE; j++) {
				D1[i - bSize][j - bSize] = a[i][j];
				D2[i - bSize][j - bSize] = b[i][j];
			}
		}

		final CompletableFuture<int[][]> cfA3 = blockAdderFunction.apply(
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(A1),
						CompletableFuture.completedFuture(A2)
				),
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(B1),
						CompletableFuture.completedFuture(C2)
				)
		);

		final CompletableFuture<int[][]> cfB3 = blockAdderFunction.apply(
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(A1),
						CompletableFuture.completedFuture(B2)
				),
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(B1),
						CompletableFuture.completedFuture(D2)
				)
		);

		final CompletableFuture<int[][]> cfC3 = blockAdderFunction.apply(
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(C1),
						CompletableFuture.completedFuture(A2)
				),
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(D1),
						CompletableFuture.completedFuture(C2)
				)
		);

		final CompletableFuture<int[][]> cfD3 = blockAdderFunction.apply(
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(C1),
						CompletableFuture.completedFuture(B2)
				),
				blockMultiplierFunction.apply(
						CompletableFuture.completedFuture(D1),
						CompletableFuture.completedFuture(D2)
				)
		);

		return cfA3.thenCompose(
				(int[][] A3) -> cfB3.thenCompose(
						(int[][] B3) -> cfC3.thenCompose(
								(int [][] C3) -> cfD3.thenApply(
										(int[][] D3) -> {
											final int[][] res = new int[MAX_MATRIX_SIZE][MAX_MATRIX_SIZE];

											for (int i = 0; i < bSize; i++) {
												for (int j = 0; j < bSize; j++) {
													res[i][j] = A3[i][j];
												}
											}
											for (int i = 0; i < bSize; i++) {
												for (int j = bSize; j < MAX_MATRIX_SIZE; j++) {
													res[i][j] = B3[i][j - bSize];
												}
											}
											for (int i = bSize; i < MAX_MATRIX_SIZE; i++) {
												for (int j = 0; j < bSize; j++) {
													res[i][j] = C3[i - bSize][j];
												}
											}
											for (int i = bSize; i < MAX_MATRIX_SIZE; i++) {
												for (int j = bSize; j < MAX_MATRIX_SIZE; j++) {
													res[i][j] = D3[i - bSize][j - bSize];
												}
											}

											return res;
										}
								)
						)
				)
		);
	}
}
