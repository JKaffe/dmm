package com.dmm.matrixmult.rest.server;

import com.dmm.matrixmult.MatrixServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GrpcServerPojo implements AutoCloseable{
	private final String ip;
	private final int port;

	private final ManagedChannel channel;
	private final MatrixServiceGrpc.MatrixServiceFutureStub stub;

	public GrpcServerPojo(String ip, int port) {
		this.ip = ip;
		this.port = port;

		Logger.getGlobal().log(Level.INFO, "Connecting to Grpc server: " + this.ip + ":" + this.port + ".");
		this.channel = ManagedChannelBuilder.forAddress(this.ip, this.port)
				.usePlaintext()
				.build();
		this.stub = MatrixServiceGrpc.newFutureStub(channel);
	}

	public String getName() {
		return this.ip + ":" + this.port;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	protected MatrixServiceGrpc.MatrixServiceFutureStub getFutureStub() {
		return stub;
	}

	@Override
	public void close() {
		this.channel.shutdown();
	}

	@Override
	protected void finalize() throws Throwable {
		this.channel.shutdownNow();
		super.finalize();
	}
}
