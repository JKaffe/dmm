package com.dmm.matrixmult.rest.server;

class MatrixRpcBinOp {
	private final int[][] a;
	private final int[][] b;

	public MatrixRpcBinOp(int[][] a, int[][] b) {
		this.a = a;
		this.b = b;
	}

	public int[][] getA() {
		return a;
	}

	public int[][] getB() {
		return b;
	}
}
